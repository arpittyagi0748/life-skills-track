### Question 1

**Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.**

**Answer :-** In the video, Angela Duckworth emphasizes the importance of perseverance and passion for success in academics and life. She stresses that success is not solely reliant on intelligence or appearance but is closely linked to resilience, treating life as a marathon rather than a sprint. Duckworth asserts that individuals with grit, who persist, are more likely to complete their education.*

### Question 2

**Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.**

**Answer :-** In the video, Professor Carol Dweck explores the concept of a growth mindset, where one believes that intelligence can be improved through effort. This mindset promotes learning and development, enabling individuals to overcome challenges. Dweck draws a distinction with a fixed mindset, where abilities are perceived as unchangeable.*

### Question 3

**What is the Internal Locus of Control? What is the key point in the video?**

**Answer :-** The Internal Locus of Control refers to the belief in one's ability to influence their life and outcomes. The video highlights a Columbia University study indicating that fifth graders attributing success to hard work tended to focus on simpler tasks and showed less persistence on challenging ones. Conversely, those attributing success to intelligence demonstrated greater motivation and perseverance. The key point is encouraging the adoption of an internal locus of control for sustained motivation.*

### Question 4

**What are the key points mentioned by the speaker to build a growth mindset?**

**Answer :-**
- *Foster Self-Belief*
- *Challenge Assumptions*
- *Cultivate a Visionary Approach*
- *Embrace Failures as Learning Opportunities*
- *Acknowledge and Navigate Challenges*

### Question 5

**What are your ideas to take action and build a Growth Mindset?**

**Answer :-** I would instill self-confidence and actively seek feedback for continuous improvement, leveraging lessons from failures as stepping stones to success. Embracing challenges with a positive outlook and perseverance would be integral to fostering a growth mindset.*
