# Question 1

**What are the steps/strategies to do Active Listening?**

Answer:

- *Maintain Focus on the Speaker and Topic:* Actively engage with the speaker and the subject matter.
- *Refrain from Interrupting:* Allow the speaker to express themselves without interruptions.
- *Utilize Open-ended Questions:* Encourage detailed responses and further discussion.
- *Take Written Notes:* Aid comprehension and retention of key points.
- *Summarize to Confirm Comprehension:* Reflect back to ensure a clear understanding.
- *Demonstrate Genuine Interest:* Show enthusiasm and engagement.
- *Practice Empathetic Responses:* Understand and respond with empathy.

# Question 2

**According to Fisher's model, what are the key points of Reflective Listening?**

Answer:

- *Validate Understanding:* Confirm the speaker's thoughts for accuracy.
- *Rooted in Empathy:* Originating from Carl Rogers' therapeutic approach, it emphasizes authentic understanding and empathy.
- *Everyday Application:* Applicable in personal relationships, especially in encouraging others to share their emotions.

# Question 3

**What are the obstacles in your listening process?**

Answer:

- *Absence of Empathy:* Lack of understanding or connection.
- *External Distractions:* Factors diverting attention away from the speaker.
- *Preconceived Notions:* Existing beliefs influencing perception.
- *Selective Focus:* Concentrating on specific aspects while neglecting others.

# Question 4

**What can you do to improve your listening?**

Answer:

- *Display Kindness and Understanding Towards Others:* Foster a positive and receptive atmosphere.
- *Minimize External Distractions:* Create an environment conducive to focused listening.
- *Refrain from Hasty Judgments:* Avoid forming opinions prematurely.
- *Demonstrate Genuine Interest in the Conversation:* Express enthusiasm and engagement.

# Question 5

**When do you switch to Passive communication style in your day to day life?**

Answer:

- *I generally lean towards a passive communication style when I aim to maintain cordial relationships and avoid potential conflicts. Although I value open communication, there are instances when prioritizing harmony takes precedence over assertiveness.*

# Question 6

**When do you switch into Aggressive communication styles in your day to day life?**

Answer:

- *In my everyday experiences, I may transition to an aggressive communication style when feeling overwhelmed or in a less favorable mood. During such moments, my expressions might become more forceful or intense than usual.*

# Question 7

**When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?**

Answer:

- *In my day-to-day life, I find myself resorting to passive-aggressive communication methods, such as sarcasm, gossiping, taunts, or employing the silent treatment, particularly when harboring frustration or resentment. This tends to occur when I struggle to express my sentiments directly.*

# Question 8

**How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? (Watch the videos first before answering this question.)**

Answer:

- *Share My Emotions:* Communicate feelings without blaming others.
- *Communicate How I Feel, Without Blaming Others:* Express emotions while avoiding assigning blame.
- *Identify My Needs Clearly:* Clearly articulate personal needs in a given situation.
- *Recognize and Practice Assertiveness in Simple Situations:* Start with straightforward scenarios to build confidence.
- *Pay Attention to Non-Verbal Cues and Tone:* Ensure body language and tone align with the spoken words.
- *Speak Up Promptly to Address Issues Proactively:* Avoid delaying communication about problems for quicker resolution.
