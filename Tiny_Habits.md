### Question 1

**In this video, what caught your attention the most, in terms of stories or ideas?**

*Answer: The standout idea here is the effectiveness of starting with small habits rather than attempting significant changes all at once. Breaking down habits into three parts – cue, routine, and reward – allows for gradual adjustments, making the formation of good habits more manageable. It's a practical approach to habit-building.*

### Question 2

**How can the B = MAP method simplify the establishment of new habits? Define M, A, and P.**

*Answer: In BJ Fogg's "Tiny Habits Method," B = MAP refers to Behavior equals Motivation, Ability, and Prompt. If I aim to initiate a new habit, say exercising, applying B = MAP involves understanding 'B' as the behavior (working out), 'M' as motivation (why I want to exercise), ensuring clarity, 'A' as ability (keeping it super easy), and 'P' as prompt (setting reminders or linking to existing routines). This formula significantly eases the process of forming new habits.*

### Question 3

**Why is it crucial to "Shine" or Celebrate after successfully completing each habit?**

*Answer: Recognizing and celebrating after completing a habit is crucial as it enhances the likability of the habit in your brain. Associating positive emotions with an activity increases the likelihood of its continuation. BJ Fogg emphasizes that by celebrating small victories, the habit becomes ingrained. Thus, incorporating a moment of happiness post-habit completion signals to your brain that the activity is rewarding, reinforcing its consistency.*

### Question 4

**In this video, what fascinated you the most, be it a story or idea?**

*Answer: The most intriguing concept revolves around making tiny improvements, like getting 1% better in every aspect of life. The illustration of a cycling team achieving significant progress through incremental changes demonstrates the cumulative power of small habits and improvements. It underscores the message that consistent, small steps can lead to substantial success over time.*

### Question 5

**What perspective does the book offer on Identity?**

*Answer: "Atomic Habits" suggests that to establish enduring habits, one should focus on becoming the type of person who engages in those habits. It extends beyond goal attainment, emphasizing the integration of habits into one's identity. The book advocates for consistent, small changes, emphasizing the importance of crafting a positive identity around your habits.*

### Question 6

**Elaborate on the book's viewpoint on making habits easier to adopt.**

*Answer: The book "Atomic Habits" recommends simplifying habits by breaking them down into four stages: cue, craving, response, and reward. It emphasizes creating an environment that makes habits attractive, satisfying immediate cravings, and ensuring they are easy to initiate and maintain. The central idea is to streamline habits, making them enjoyable and increasing the likelihood of successful habit formation.*

### Question 7

**Discuss the book's perspective on making habits more challenging.**

*Answer: While the book "Atomic Habits" doesn't explicitly address making habits more difficult, it suggests focusing on creating an environment that facilitates good habits and hinders bad ones. For example, to curb snacking, keeping unhealthy snacks out of reach makes it a bit tougher to engage in the undesirable habit. The emphasis is on structuring surroundings to aid positive habits and present minor obstacles for negative ones.*

### Question 8:

**Select a habit you want to increase. Outline steps to make the cue obvious, the habit more attractive or easy, and the response satisfying.**

*Answer:*
**Habit: Reading More**

**Obvious Cue:**
- Set a dedicated daily reading time.
- Place the book on the bedside table.

**Attractive Habit:**
- Choose books in genres I enjoy.
- Create a cozy reading nook.

**Easy Habit:**
- Begin with short reading sessions.
- Always have a book easily accessible.

**Satisfying Response:**
- Celebrate finishing a book.
- Reward myself with a favorite treat after reading.

### Question 9:

**Identify a habit you want to reduce. Outline steps to make the cue invisible, the process unattractive or hard, and the response unsatisfying.**

*Answer:*
**Habit: Excessive Social Media Usage**

**Invisible Cue:**
- Disable notifications on social media apps.
- Set specific, limited times for checking.

**Unattractive Process:**
- Educate myself on the negative impacts of excessive usage.
- Visualize engaging in more fulfilling activities.

**Hard Process:**
- Move social media apps to a folder, increasing the steps to access them.
- Set app usage limits on the phone.

**Unsatisfying Response:**
- Reflect on how I feel after extended social media use.
- Reward myself with a healthier alternative after avoiding excessive usage.
