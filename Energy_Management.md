### Question 1

**What are the activities you do that help you relax - Calm quadrant?**

*Answer:*

- **Deep Breathing:** Engage in slow, deep breaths to induce relaxation.
- **Nature Walks:** Take leisurely strolls in parks or natural settings for calming effects.
- **Listening to Music:** Enjoy soothing melodies to promote relaxation.
- **Hot Baths/Showers:** Take warm baths or showers to release tension and unwind.
- **Journaling:** Write down thoughts and feelings to clear the mind and alleviate stress.
- **Digital Detox:** Take breaks from technology and work to promote mental peace and relaxation.

### Question 2:

**When do you find yourself entering the Stress quadrant?**

*Answer:*

- Learning new things.
- Engaging in intense workouts.
- Facing challenging tasks or high expectations.
- Dealing with tight deadlines or unexpected changes.
- Handling conflicts or tension.

### Question 3:

**How do you understand if you're in the Excitement quadrant?**

*Answer:*

- Feeling full of energy and vitality.
- Demonstrating enthusiasm through words and actions.
- Increased frequency of smiling and laughter.
- Being focused and engaged in activities.
- Maintaining a positive attitude.
- Being open to new ideas and opportunities.

### Question 4

**Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.**

*Answer:*

- Sleep is essential for physical and mental health.
- Inadequate sleep can have detrimental effects on health, including increased heart rates and accidents.
- Poor sleep weakens the immune system, making individuals more susceptible to infections.
- Sleep plays a crucial role in memory, learning, and overall well-being.
- Disrupted gene activity occurs with insufficient sleep.
- Consistent sleep patterns and maintaining a cool bedroom environment improve sleep quality.

### Question 5

**What are some ideas that you can implement to sleep better?**

*Answer:*

- Establish a consistent sleep schedule by going to bed and waking up at the same time daily.
- Create a conducive sleep environment by keeping the bedroom cool, dark, and quiet.
- Avoid screens before bedtime to reduce exposure to blue light, which can disrupt sleep.
- Refrain from consuming heavy meals and excessive drinks close to bedtime to prevent discomfort and sleep disturbances.

### Question 6

**Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.**

*Answer:*

- Even minimal exercise boosts mood, attention, and memory.
- Regular exercise provides long-term benefits for brain health.
- Exercise acts as a protective factor against neurodegenerative diseases.
- Aim for 30 minutes of daily exercise, incorporating aerobic activities.
- Short bursts of exercise, even as brief as one minute, contribute to brain health.

### Question 7

**What are some steps you can take to exercise more?**

*Answer:*

- Strive to achieve at least 30 minutes of physical activity every day.
- Include aerobic exercises in your routine for cardiovascular health.
- Start with small, manageable exercise sessions to build consistency.
- Engage in activities that you enjoy to make exercise more enjoyable and sustainable.
- Set realistic goals to maintain motivation and track progress.
- Make exercise a social activity by partnering with friends or family members for workouts.
