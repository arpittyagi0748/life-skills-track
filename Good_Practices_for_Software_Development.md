### Question 1

**Which point(s) were new to you?**

*Answer:*

**Novel Points:**
- **Utilizing Tools for Distraction Management:** Incorporating tools like TimeLimit and Freedom to eliminate notifications during work hours was a new concept for me.
- **Importance of Availability:** Recognizing the significance of being available when someone replies to messages was an insightful point.

### Question 2

**Which area do you think you need to improve on? What are your ideas to make progress in that area?**

*Answer:*

**Area for Enhancement:**
- **Enhancing Team Communication in Meetings**
- **Taking Effective Notes during Team Discussions**

**Strategies for Improvement:**
- **Active Participation:** Engage more actively in meetings by asking pertinent questions to facilitate understanding and collaboration.
- **Leveraging Group Chat:** Utilize group chat platforms more effectively to maintain continuous communication and clarity among team members.
- **Clear Communication:** Ensure that problems and requirements are explained clearly during discussions, possibly incorporating visual aids such as pictures and videos for better comprehension.
- **Utilizing Collaborative Tools:** Share code snippets using platforms like Github gists and utilize sandbox tools for collaborative coding sessions.
